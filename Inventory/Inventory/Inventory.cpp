#include "Inventory.h"

map<string, int>* inventory;

void Inventory::Print()
{
	cout << endl;
	for (map<string, int>::iterator i = (*inventory).begin(); i != (*inventory).end(); ++i)
	{
		cout << "Item: " << i->first << " quantity: " << i->second << endl;
	}
}

void Inventory::Add(string itemName)
{
	(*inventory)[itemName] += 1;
}

void Inventory::UseItem(string itemName)
{
	if (inventory->count(itemName) == 1)
	{
		int amount = (*inventory)[itemName];

		if (amount == 1)
		{
			inventory->erase(itemName);
		}
		else
		{
			(*inventory)[itemName] -= 1;
		}
	}
}

Inventory::Inventory()
{
	inventory = new map<string, int>();
}

Inventory::~Inventory()
{
	delete inventory;
}