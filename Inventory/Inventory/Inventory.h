#pragma once

#include <string>
#include <map>
#include <iostream>

using namespace std;

class Inventory
{
public:
	Inventory();
	~Inventory();
	void Print();
	void Add(string itemName);
	void UseItem(string itemName);
};