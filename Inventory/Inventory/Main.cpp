#include <iostream>
#include <string>
#include "Inventory.h"

using namespace std;

int main()
{


	int input;
	string itemName;
	Inventory* inventory = new Inventory();

	while (true)
	{
		cout << "Commands:" << endl;
		cout << "1. Add Item" << endl;
		cout << "2. Remove Item" << endl;
		cout << "3. Show full inventory" << endl;
		cout << "4. Exit" << endl;

		cin >> input;

		if (input == 4)
		{
			break;
		}

		if (input == 1)
		{
			cout << "Type name of the item that should be added." << endl;
			cin >> itemName;
			inventory->Add(itemName);
			inventory->Print();
		}
		else if (input == 2)
		{
			cout << "Type name of the item that should be removed." << endl;
			cin >> itemName;
			inventory->UseItem(itemName);
			inventory->Print();
		}
		else if (input == 3)
		{
			inventory->Print();
		}
	}
}